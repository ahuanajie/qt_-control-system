#include "QtEarthStation.h"
#include<qpushbutton.h>

QtEarthStation::QtEarthStation(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	//QPushButton *btn = new QPushButton(this);

	ui.groupBox_sbkz->setVisible(false);
}

//点击全局监控事件
void QtEarthStation::on_pbt_qjjk_clicked() {

	ui.GroupBox_qjjk->setVisible(true);
	
	ui.groupBox_sbkz->setVisible(false);
	
}

//点击设备控制事件
void QtEarthStation::on_pbt_sbkz_clicked() {

	ui.GroupBox_qjjk->setVisible(false);

	ui.groupBox_sbkz->setVisible(true);

}

//点击设备监控事件
void QtEarthStation::on_pbt_sbjk_clicked() {

	ui.GroupBox_qjjk->setVisible(true);

	ui.groupBox_sbkz->setVisible(false);

}

//点击配置调试事件
void QtEarthStation::on_pbt_pzts_clicked() {

	ui.GroupBox_qjjk->setVisible(true);

	ui.groupBox_sbkz->setVisible(false);

}
