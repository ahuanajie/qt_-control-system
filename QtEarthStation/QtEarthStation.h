#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_QtEarthStation.h"

class QtEarthStation : public QMainWindow
{
	Q_OBJECT

public:
	QtEarthStation(QWidget *parent = Q_NULLPTR);

private slots:

	void on_pbt_qjjk_clicked(); //全局监控按钮事件
	void on_pbt_sbkz_clicked();//全局设备控制事件
	void on_pbt_sbjk_clicked();//全局设备监控事件
	void on_pbt_pzts_clicked();//全局配置调试事件


private:
	Ui::QtEarthStationClass ui;
};
